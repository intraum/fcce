#AutoIt3Wrapper_AU3Check_Parameters=-d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
#include-once
#include <WinAPIDlg.au3>


Func _GUICtrlChkboxRadRead($hControl)

	Return Int(GUICtrlRead($hControl)) = 1 ? 1 : 0

EndFunc   ;==>_GUICtrlChkboxRadRead


; получение id элемента для ф-ций GUI;
; второй параметр в формате https://www.autoitscript.com/autoit3/docs/intro/controls.htm

Func _GUIGetCtrlID(ByRef Const $hWnd, $sControl)

	Return _WinAPI_GetDlgCtrlID(ControlGetHandle($hWnd, '', $sControl))

EndFunc   ;==>_Gui_GetCtrlId


Func _MsgBoxFireExit($iFlag, $sTitle, $sMess, $hWnd = 0)

	If Not $hWnd Then

		MsgBox($iFlag, $sTitle, $sMess)
	Else
		MsgBox($iFlag, $sTitle, $sMess, $hWnd)
	EndIf

	Exit

EndFunc   ;==>_MsgBoxFireExit


Func _SleepRandom($iMin = 1, $iMax = 2, $iFlag = 1)

	Local $sAppend

	Switch $iFlag
		Case 1
			$sAppend = '000'
		Case 2
			$sAppend = '00'
		Case 3
			$sAppend = '0'
		Case Else
			$sAppend = '000'
	EndSwitch

	Sleep(Random($iMin, $iMax, 1) & $sAppend)

EndFunc   ;==>_SleepRandom
